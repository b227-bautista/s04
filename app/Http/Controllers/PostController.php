<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    
    public function store(Request $request){
        if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('login');
        }
    }

    // action that will return a view showing all blog posts.
    public function index(){
        $posts = Post::all()->where('isActive', 1);
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome(){
        $posts = Post::all();
        return view('welcome')->with('posts', $posts->random(3));
    }

    //action for showing only the posts authored by authenticated user.
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }
    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);

            return view('posts.edit')->with('post', $post);
        }
        else{
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id)
            {
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                $post->save();
            }
        
            return redirect('/posts');       
    }

    public function destroy($id){
        $post = Post::find($id);

        //if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id)
            {
                $post->delete();
            }

        return redirect('/posts');
    }

    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id)
            {
                $post->isActive = false;
                $post->save();
            }

        return redirect('/posts');
    }

    public function unArchive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id)
            {
                $post->isActive = true;
                $post->save();
            }

        return redirect('/posts');
    }
}

